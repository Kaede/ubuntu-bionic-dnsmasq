ubuntu-bionic-dnsmasq
=========

Ubuntu 18.04 LTS (Bionic Beaver)のローカルDNSリゾルバーをsystemd-resolvedからdnsmasqへ変更するAnsible Role。

Requirements
------------

### Ansible Version
- Ansible 2.5.2

### Distribution
- Ubuntu 18.04 LTS

Role Variables
--------------

N/A

Dependencies
------------

N/A

Example Playbook
----------------

```
- hosts: bionic
  roles
    - ubuntu-bionic-dnsmasq
```

License
-------

GPLv3

Author Information
------------------

- Twitter: [@__Kaede](https://twitter.com/__Kaede)
